"""
    STRipy
    Copyright (C) 2021  Andreas Halman

    STRipy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    STRipy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with STRipy.  If not, see <http://www.gnu.org/licenses/>.
"""


import io
import json
from datetime import date
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_CENTER, TA_RIGHT
from reportlab.platypus import Paragraph, Table, TableStyle, SimpleDocTemplate, Image, Spacer, KeepTogether
from reportlab.graphics.shapes import Drawing, Line
from reportlab.lib.pagesizes import portrait, A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib import colors
from reportlab.lib.units import inch, cm, mm
from reportlab.graphics import renderPDF
from svglib.svglib import svg2rlg

_baseFontName  ='Helvetica'
_baseFontNameB ='Helvetica-Bold'
_baseFontNameI ='Helvetica-Oblique'
_baseFontNameBI='Helvetica-BoldOblique'

# Scale a drawing using the given scaling factor
def scale(drawing, scaling_factor):
    drawing.width = drawing.minWidth() * scaling_factor
    drawing.height = drawing.height * scaling_factor
    drawing.scale(scaling_factor, scaling_factor)

    return drawing


# Resize visualised reads SVG so it would fit to A4 page
def resizeSVG(file_path):
    drawing = svg2rlg(file_path)

    pixel_mm_ratio = 0.352733686067019 # at 72 dpi
    a4_w_mm = 180
    a4_h_mm = 260

    drawing_w_px = drawing.width
    drawing_w_mm = drawing_w_px * pixel_mm_ratio
    scaling_factor_w = a4_w_mm / drawing_w_mm

    drawing_h_px = drawing.height
    drawing_h_mm = drawing_h_px * pixel_mm_ratio
    scaling_factor_h = a4_h_mm / drawing_h_mm

    scaling_factor = min([scaling_factor_w, scaling_factor_h])

    scaled_drawing = scale(drawing, scaling_factor = scaling_factor)

    return scaled_drawing


# Convert SVG to PDF
def convertSVGtoPDF(input_svg):
    pdf_buffer = io.BytesIO()
    drawing = svg2rlg(input_svg)
    renderPDF.drawToFile(drawing, pdf_buffer)
    pdf_content = pdf_buffer.getvalue()
    pdf_buffer.close()

    return pdf_content


# Generate the PDF report
def createPDFreport(data, input_svg):
    inheritance_types = {
        "NI": "Not inherited",
        "AD": "Autosomal dominant",
        "AR": "Autosomal recessive",
        "XLD": "X-linked dominant",
        "XLR": "X-linked recessive",
        "AD or AR": "Autosomal dominant or recessive"
    }

    # Set page parameters
    styles = getSampleStyleSheet()

    pdf_buffer = io.BytesIO()
    doc = SimpleDocTemplate(pdf_buffer, pagesize = portrait(A4), rightMargin = 20, leftMargin = 35, topMargin = 20, bottomMargin = 10, title = "Genotyping report", author = "STRipy")
    content = []

    # Date of the analysis
    today = date.today()
    time_printed = today.strftime("%B %d, %Y")

    # Styles
    style_text_black = "#2e2e2e"
    style_text_lightblack = "#4f4f4f"
    style_text_darkblue = "#265069"
    style_text_lightblue = "#006ead"
    style_text_extralightblue = "#92bad1"
    style_text_extralightblack = "#bdbdbd"

    style_title = ParagraphStyle(name='Heading1',
                        parent=styles['Normal'],
                        fontName = _baseFontName,
                        fontSize=16,
                        leading=22,
                        textColor=colors.HexColor(style_text_black),
                        spaceAfter=6,
                        alignment=TA_CENTER)

    style_smallheader = ParagraphStyle(name='Heading3',
                        parent=styles['Normal'],
                        fontName = _baseFontNameB,
                        fontSize=12,
                        textColor=colors.HexColor(style_text_lightblue),
                        leading=14,
                        spaceAfter=1,
                        alignment=TA_LEFT)

    style_timeprinted = ParagraphStyle(name='Normal',
                        parent=styles['Normal'],
                        fontName = _baseFontName,
                        fontSize=10,
                        textColor=colors.HexColor(style_text_black),
                        alignment=TA_LEFT)

    style_body = ParagraphStyle(name='Normal',
                        parent=styles['Normal'],
                        fontName = _baseFontName,
                        fontSize=10,
                        textColor=colors.HexColor(style_text_black),
                        alignment=TA_LEFT)

    style_key = ParagraphStyle(name='NormalBold',
                        parent=styles['Normal'],
                        fontName = _baseFontNameB,
                        fontSize=10,
                        textColor=colors.HexColor(style_text_darkblue),
                        alignment=TA_LEFT)

    style_key_sub = ParagraphStyle(name='NormalBoldSub',
                        parent=styles['Normal'],
                        fontName = _baseFontName,
                        fontSize=10,
                        leading=10,
                        textColor=colors.HexColor(style_text_darkblue),
                        alignment=TA_LEFT)

    style_val = ParagraphStyle(name='NormalVal',
                        parent=styles['Normal'],
                        fontName = _baseFontName,
                        fontSize=10,
                        textColor=colors.HexColor(style_text_black),
                        alignment=TA_LEFT)

    style_allele = ParagraphStyle(name='NormalVal',
                        parent=styles['Normal'],
                        fontName = _baseFontNameB,
                        fontSize=18,
                        leading=15,
                        textColor=colors.HexColor(style_text_black),
                        alignment=TA_CENTER)

    style_allele_extra = ParagraphStyle(name='NormalVal',
                        parent=styles['Normal'],
                        fontName = _baseFontName,
                        fontSize=10,
                        leading=10,
                        textColor=colors.HexColor(style_text_lightblack),
                        alignment=TA_CENTER)

    # Create the page
    # Logo and date in header
    stripy_logo = scale(svg2rlg('logo.svg'), 0.2)

    header_data = [[Image(stripy_logo, hAlign="LEFT"), Paragraph(time_printed, style_timeprinted)]]
    header = Table(header_data, colWidths=[158 * mm, 34 * mm])
    content.append(header)

    content.append(Spacer(1, 30))

    # Title
    title = Paragraph("GENOTYPING REPORT", style_title)
    content.append(title)
    content.append(Spacer(1, 20))

    # Horizontal line
    drawline = Drawing(505, 1)
    horizontal_line = Line(0, 0, 505, 0)
    horizontal_line.strokeColor = colors.HexColor(style_text_extralightblue)
    drawline.add(horizontal_line)

    # Variant (locus) information section
    content.append(Paragraph("Locus information", style_smallheader))
    content.append(drawline)
    content.append(Spacer(1, 5))

    table_variant_data = []
    location_region_addition = ' (' + data["VariantInfo"]["LocationRegion"] + ')' if data["VariantInfo"]["Locus"] != "Custom" else ''
    table_variant_data.append([Paragraph("Locus:", style_key), Paragraph(data["VariantInfo"]["Locus"], style_val)])
    table_variant_data.append([Paragraph("Repeat unit:", style_key), Paragraph(data["VariantInfo"]["Motif"], style_val)])
    table_variant_data.append([Paragraph("Reference:", style_key), Paragraph(data["SampleResults"]["AnalysedReferenceCoordinates"] + location_region_addition, style_val)])

    if (data["VariantInfo"]["Locus"] != "Custom"):
        table_variant_data.append([Paragraph("Disease:", style_key), Paragraph(data["VariantInfo"]["Diseases"][data["SampleResults"]["AnalysedDisease"]]["DiseaseName"], style_val)])
        table_variant_data.append([Paragraph("Inheritance:", style_key), Paragraph(inheritance_types[data["VariantInfo"]["Diseases"][data["SampleResults"]["AnalysedDisease"]]["Inheritance"]], style_val)])

    if (data["VariantInfo"]["Locus"] != "Custom"):
        repeat_range_normal = data["VariantInfo"]["Diseases"][data["SampleResults"]["AnalysedDisease"]]["NormalRange"]
        if repeat_range_normal["Min"] == repeat_range_normal["Max"]:
            repeat_range_normal_text = 'Normal ' + str(repeat_range_normal["Max"]) + '; '
        else:
            if repeat_range_normal["Min"] == 0:
                repeat_range_normal_text = 'Normal ≤' + str(repeat_range_normal["Max"]) + '; '
            else:
                repeat_range_normal_text = 'Normal ' + str(repeat_range_normal["Min"]) + '-' + str(repeat_range_normal["Max"]) + '; '
                
        repeat_range_intermed = data["VariantInfo"]["Diseases"][data["SampleResults"]["AnalysedDisease"]]["IntermediateRange"]
        path_range_min = int(data["VariantInfo"]['Diseases'][data["SampleResults"]["AnalysedDisease"]]["PathogenicCutoff"]) if data["VariantInfo"]["Locus"] != 'FMR1' else int(data["VariantInfo"]['Diseases'][data["SampleResults"]["AnalysedDisease"]]["PathogenicCutoff"].split("-")[0]) # Quick FMR1 fix
        repeat_range_path_text = 'Pathogenic ≥' + str(path_range_min)

        if repeat_range_intermed == 'NA':
            repeat_range_intermed_text = ''
        else:
            repeat_range_intermed_text = 'Intermediate: ' + str(repeat_range_intermed["Min"]) + '-' + str(repeat_range_intermed["Max"]) + '; '

        table_variant_data.append([Paragraph("Repeat ranges:", style_key), Paragraph(repeat_range_normal_text + repeat_range_intermed_text + repeat_range_path_text, style_val)])


    variant_table = Table(table_variant_data, colWidths=[36 * mm, 140 * mm])
    variant_table.setStyle(TableStyle([
                                    ('ALIGN', (0, 0), (-1, -1), "LEFT"),
                                    ('TEXTCOLOR', (0, 0), (-1, -1), colors.HexColor(style_text_black))
                                ]))
    content.append(variant_table)
    content.append(Spacer(1, 30))

    # Genotyping results section
    content.append(Paragraph("Genotyping results", style_smallheader))
    content.append(drawline)
    content.append(Spacer(1, 8))

    table_sample_data1 = []
    allele1_totalreads = str(data["SampleResults"]["TotalOfSpanningReads"][0]) + ' spanning; ' + str(data["SampleResults"]["TotalOfFlankingReads"][0]) + ' flanking; ' + str(data["SampleResults"]["TotalOfInrepeatReads"][0]) + ' in-repeat'
    allele2_totalreads = str(data["SampleResults"]["TotalOfSpanningReads"][1]) + ' spanning; ' + str(data["SampleResults"]["TotalOfFlankingReads"][1]) + ' flanking; ' + str(data["SampleResults"]["TotalOfInrepeatReads"][1]) + ' in-repeat'

    # Show only one allele if we analyse chromosome X or Y and it is a male sample
    if (data["SampleResults"]["SampleSex"] == "male" and ("X" in data["SampleResults"]["AnalysedReferenceCoordinates"] or "Y" in data["SampleResults"]["AnalysedReferenceCoordinates"])):
        table_sample_data1.append([Paragraph("Genotype:", style_key),
                                Paragraph(str(data["SampleResults"]["Allele1Len"]), style_allele)])

        table_sample_data1.append([Paragraph("Confidence interval:", style_key_sub),
                                Paragraph('(' + data["SampleResults"]["Allele1CI"] + ')', style_allele_extra)])

        table_sample_data1.append([Paragraph("Reads consistent:", style_key_sub),
                                Paragraph(allele1_totalreads, style_allele_extra)])

    else:
        table_sample_data1.append([Paragraph("Genotype:", style_key),
                                Paragraph(str(data["SampleResults"]["Allele1Len"]), style_allele), 
                                Paragraph(str(data["SampleResults"]["Allele2Len"]), style_allele)])

        table_sample_data1.append([Paragraph("Confidence interval:", style_key_sub),
                                Paragraph('(' + data["SampleResults"]["Allele1CI"] + ')', style_allele_extra), 
                                Paragraph('(' + data["SampleResults"]["Allele2CI"] + ')', style_allele_extra)])

        table_sample_data1.append([Paragraph("Reads consistent:", style_key_sub),
                                Paragraph(allele1_totalreads, style_allele_extra), 
                                Paragraph(allele2_totalreads, style_allele_extra)])

    table_sample_data1.append(['', '', ''])

    sample_table1 = Table(table_sample_data1, colWidths=[36 * mm, 70 * mm, 70 * mm])
    sample_table1.setStyle(TableStyle([
                                    ('ALIGN', (0, 0), (-1, -1), "LEFT"),
                                    ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                    ('TEXTCOLOR', (0, 0), (-1, -1), colors.HexColor(style_text_black)),
                                ]))

    table_sample_data2 = []
    table_sample_data2.append([Paragraph("Extra:", style_key), 
                            Paragraph('Read length: ' + str(data["SampleResults"]["ReadLength"]) + ' bp', style_val),
                            Paragraph('Fragment length: ' + str(data["SampleResults"]["FragmentLength"]) + ' bp', style_val),
                            ])

    if data["SampleResults"]["MatesFullOfRepeats"] != '':
        mates_full_of_repeats = data["SampleResults"]["MatesFullOfRepeats"]
    else:
        mates_full_of_repeats = 'NA'
  
  
    if (data["VariantInfo"]["Locus"] != "Custom"):
        table_sample_data2.append(["", 
                                Paragraph('Coverage: ' + str(data["SampleResults"]["Coverage"]) + 'x', style_val),
                                Paragraph('Mates full of repeats: ' + str(mates_full_of_repeats), style_val)
                                ])

        flanking_repeats = ''
        flanking_first_element = True

        if data["SampleResults"]["HighestPathRepeatsInFlanking"] != "0":
            highest_repeats = dict(json.loads(data["SampleResults"]["HighestPathRepeatsInFlanking"]).items())
            highest_repeats = dict(sorted(highest_repeats.items(), key=lambda x: int(x[0]))[:7]) # Sort items based on key value (convert to int) and get max 7 items

            for key, value in highest_repeats.items():
                flanking_repeats += str(key) + '<sub>(' + str(value) + ')</sub> ' 
                flanking_first_element = False if flanking_first_element == True else False
        else:
            flanking_repeats = 0

        table_sample_data2.append(["", 
                                Paragraph('Pathogenic repeats found in sp. & fl. reads:', style_val),
                                Paragraph(str(flanking_repeats), style_val)
                                ])
        
    else:
        table_sample_data2.append(["", 
                                Paragraph('Coverage: ' + str(data["SampleResults"]["Coverage"]) + 'x', style_val)
                                ])

    table_sample_data2.append(["", '', ''])
  

    sample_table2 = Table(table_sample_data2, colWidths=[40 * mm, 72 * mm, 64 * mm])
    sample_table2.setStyle(TableStyle([
                                    ('ALIGN', (0, 0), (-1, -1), "LEFT"),
                                    ('TEXTCOLOR', (0, 0), (-1, -1), colors.HexColor(style_text_black))
                                ]))
    content.append(sample_table1)

    drawline_black = Drawing(405, 1)
    horizontal_line_black = Line(20, 0, 505, 0)
    horizontal_line_black.strokeColor = colors.HexColor(style_text_extralightblack)
    drawline_black.add(horizontal_line_black)
    content.append(drawline_black)
    content.append(Spacer(1, 10))

    content.append(sample_table2)

    content.append(Paragraph("Read alignments", style_smallheader))
    content.append(drawline)
    content.append(Spacer(1, 5))

    reads_visualising_plot = resizeSVG(input_svg)
    content.append(KeepTogether(reads_visualising_plot))
    doc.multiBuild(content)

    pdf_content = pdf_buffer.getvalue()
    pdf_buffer.close()

    return pdf_content