#!/bin/bash

# File paths
path_root=.
path_bwa=bwa
path_samtools=samtools
path_expansionhunter=$path_root/bin/ExpansionHunter
path_reviewer=$path_root/bin/REViewer
path_ref_fasta_folder=$path_root/reference/genomes
path_results=$path_root/results

# Input variables
locus_name=$1
file_path=$2
file_uuid=$3
genome=$4
sex=$5
offtarget_file=$6
req_genome_has_chr=$7
analysis_type=$8
region_extension=$9

if [ "$locus_name" == "XYLT1" ]
then
	if [ "$analysis_type" == "extended" ]
	then
		offtarget_file="reference/varcats/XYLT1_extended.json"
	else
		offtarget_file="reference/varcats/XYLT1.json"
	fi

	temp_files_path=results/temp
	reads=$temp_files_path/$file_uuid.xylt1mapped.bam
	genome_fasta=XYLT1/xylt1_ref.fa

	cd $path_root/

	$path_samtools index $file_path

	python3 xylt1-extraction.py $file_path $temp_files_path/

	$path_bwa mem -aM -A 1 -B 6 -O 4 $path_ref_fasta_folder/XYLT1/xylt1_ref.fa $temp_files_path/$file_uuid.regions_R1.fq $temp_files_path/$file_uuid.regions_R2.fq | \
	$path_samtools sort - | \
	$path_samtools view -Sb > $temp_files_path/$file_uuid.xylt1mapped.bam

	$path_samtools index $temp_files_path/$file_uuid.xylt1mapped.bam

else
	reads=$file_path

	if [ "$req_genome_has_chr" == "False" ]
	then
		genome_fasta=${genome}_nochr.fa
	else
		genome_fasta=$genome.fa
	fi
fi


if [[ -f "$path_expansionhunter" ]] && [[ -f "$path_reviewer" ]] && [[ -f "$path_ref_fasta_folder/$genome_fasta" ]]; then
	cd $path_root/

	if [[ -f "$reads" ]]; then
		# Run ExpansionHunter to genotype the sample
		$path_expansionhunter \
			--sex $sex \
			--reads $reads \
			--region-extension-length $region_extension \
			--reference $path_ref_fasta_folder/$genome_fasta \
			--variant-catalog $offtarget_file \
			--output-prefix results/$file_uuid
	else
		echo "$reads file does not exist."
	fi

	if [[ -f "$path_results/${file_uuid}_realigned.bam" ]]; then
		# Prepare BAMlet for Reviewer and run it to generate read visualisations
		$path_samtools sort -m 256M $path_results/${file_uuid}_realigned.bam > $path_results/${file_uuid}_realigned.sorted.bam
		$path_samtools index $path_results/${file_uuid}_realigned.sorted.bam

		$path_reviewer \
			--reads $path_results/${file_uuid}_realigned.sorted.bam \
			--vcf $path_results/${file_uuid}.vcf \
			--region-extension-length $region_extension \
			--reference $path_ref_fasta_folder/$genome_fasta \
			--catalog $offtarget_file \
			--locus $locus_name \
			--output-prefix $path_results/${file_uuid}
	else
		echo "$path_results/${file_uuid}_realigned.bam file does not exist."
	fi

else
	echo "Some tools or reference files are missing."
fi
